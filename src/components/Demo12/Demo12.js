import React, {Component} from 'react'

import $ from 'jquery'

class Demo12 extends Component {
    constructor(props){
        super(props);
        this.state = {
            loading: true,
            error: null,
            data: null
        }
    }
    componentDidMount(){
        this.props.promise.then(
            value => {this.setState({loading:false,data:value})},
            error => {this.setState({loading:true,data:error})}
        )
    }

    render() {
        if(this.state.loading){
            return <span>loading</span>
        }else if(this.state.error !== null){
            return <span> Error : {this.state.error.message}</span>
        }else {
            var repos = this.state.data.items
            var repoList = repos.map(function (item ,index) {
                return <li key={index}>{item.name} </li>
            })


            return (
                <div>
                   <ul>
                       {repoList}
                   </ul>
                </div>
            )
        }

    }
}
export default Demo12