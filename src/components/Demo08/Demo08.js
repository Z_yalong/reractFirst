import React, {Component} from 'react'

class Demo08 extends Component {
    constructor(props){
        super(props);
        this.state = {
            liked: false
        }
    }
    togoless (){
        this.setState({liked : !this.state.liked})
    }
    render() {
        var text = this.state.liked ? 'like' :'shite'
        return (
           <div>
               <p>this state is : {text}</p>
               <button onClick={this.togoless.bind(this)}>文字切换</button>
           </div>
        )
    }
}
export default Demo08