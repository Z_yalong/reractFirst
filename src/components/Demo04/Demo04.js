import React, {Component} from 'react'

class Demo04 extends Component{
    render(){
        return (
            <h1>the props : {this.props.name}</h1>
        )
    }
}

export default Demo04