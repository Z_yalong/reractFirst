import React, {Component} from 'react'

var arr = [
    <h1 key='1'>this is arr[1]</h1>,
    <h1 key='2'>this is arr[12]</h1>
]


class Demo03 extends Component{
    render(){
        return (
            <div>
                {arr}
            </div>
        )
    }
}

export default Demo03