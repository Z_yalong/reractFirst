import React, {Component} from 'react'
import PropTypes from 'prop-types';

class Demo06 extends Component {

    static propTypes = {
        title:PropTypes.string.isRequired,
    };

    render() {
        let {title} = this.props;
        return (
            <h1>{this.props.title}</h1>
        )
    }
}
export default Demo06