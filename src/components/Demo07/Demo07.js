import React, {Component} from 'react'

class Demo07 extends Component {
    fousInp (){
        this.refs.eleInput.focus()
    }
    render() {
        return (
           <div>
               <input ref='eleInput' type="text"/>
               <button onClick={this.fousInp.bind(this)}>获取焦点</button>
           </div>
        )
    }
}
export default Demo07