import React, {Component} from 'react'

class Demo09 extends Component {
    constructor(props){
        super(props);
        this.state = {
            text: ''
        }
    }
    togoless (event){
        this.setState({text: event.target.value});
    }
    render() {
        var value = this.state.text
        return (
           <div>
               <input type="text"  onChange={this.togoless.bind(this)}/>
               <p>{value}</p>
           </div>
        )
    }
}
export default Demo09