import React, {Component} from 'react'

import $ from 'jquery'

class Demo11 extends Component {
    constructor(props){
        super(props);
        this.state = {
            username: '',
            lastGistUrl: ''
        }
    }
    componentDidMount(){
       $.get(this.props.source, function (result) {
           console.log(result);
           var lastGist = result[0]
           this.setState({
               username:lastGist.owner.login,
               lastGistUrl: lastGist.html_url
           })
       }.bind(this))
    }

    render() {

        return (
           <div>
               {this.state.username} : super <br/>
               <a href={this.state.lastGistUrl}>here</a>
           </div>
        )
    }
}
export default Demo11