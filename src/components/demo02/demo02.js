import React ,{Component} from 'react'

var names = ['aaaa' , 'bbbb' , 'cccc']
class Demo02 extends Component {
    render( ) {
        return (
            <ul>
                {names.map(function (item , index) {
                    return <li key={index}> {index}  : {item}</li>
                })}
            </ul>
        )
    }
}
export default Demo02