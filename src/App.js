import React, {Component} from 'react';
import $ from  'jquery'
import Demo01 from './components/demo01/demo01'
import Demo02 from './components/demo02/demo02'
import Demo03 from './components/demo03/Demo03'
import Demo04 from './components/Demo04/Demo04'
import Demo05 from './components/Demo05/Demo05'
import Demo06 from './components/Demo06/Demo06'
import Demo07 from './components/Demo07/Demo07'
import Demo08 from './components/Demo08/Demo08'
import Demo09 from './components/Demo09/Demo09'
import Demo10 from './components/Demo10/Demo10'
import Demo11 from './components/Demo11/Demo11'
import Demo12 from './components/Demo12/Demo12'
var data = '123456'
class App extends Component {
    render() {
        return (
            <div>
                {/*<Demo01/>*/}
                {/*/!*<hr/>*!/*/}
                {/*/!*<Demo02/>*!/*/}
                {/*<hr/>*/}
                {/*<Demo03/>*/}
                {/*<hr/>*/}
                {/*<Demo04 name='萨达萨达所多'/>*/}
                {/*<hr/>*/}
                {/*<Demo05 >*/}
                    {/*<span>children1</span>*/}
                    {/*<span>children2</span>*/}
                {/*</Demo05>*/}
                {/*<hr/>*/}
                {/*<Demo06 title={data}/>*/}
                {/*<hr/>*/}
                {/*<Demo07 />*/}
                {/*<hr/>*/}
                {/*<Demo08 />*/}
                {/*<hr/>*/}
                {/*<Demo09 />*/}
                {/*<hr/>*/}
                {/*<Demo10 />*/}
                {/*<hr/>*/}
                {/*<Demo11 source='https://api.github.com/users/octocat/gists'/>*/}
                <Demo12 promise={$.getJSON('https://api.github.com/search/repositories?q=javascript&sort=stars')} />
            </div>
        );
    }
}

export default App;
